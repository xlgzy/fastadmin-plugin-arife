<?php

namespace addons\siteconfig;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Siteconfig extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        
        return true;
    }

    /**
     * 插件注册路由
     *
     * @return void
     * @author AriFe.Liu 
     */
    public function appInit(){
        $config = get_addon_config('siteconfig');
        if(isset($config['pages']) && !empty($config['pages'])){
            $page_arr = json_decode($config['pages'],true);
            foreach ($page_arr as $key => $value) {
                \think\Route::rule($value['rewrite'],$value['mca']);
            }
        }
    }

    /*
        # 配置使用示例:

        // 将所有配置赋值到模板变量
        $conf = get_addon_config('siteconfig');
        $this->view->assign('conf',$conf);

        // 如果有配置页面, 则根据路由信息匹配当前页面标题等
        // 前端生成链接时, 务必使用 {:url('index/user/index')} 类似方法来根据路由自动生成地址, 否则可能无法匹配
        if(isset($conf['pages']) && !empty($conf['pages'])){
            $routeInfo = $this->request->routeInfo();            
            $mca = $routeInfo['route'];

            $pages = json_decode($conf['pages'],true);

            // title
            $titles = array_column($pages,'title','mca');
            if(isset($titles[$mca])){
                $this->view->assign('title',$titles[$mca]);
            }
            // description
            $descs = array_column($pages,'desc','mca');
            if(isset($descs[$mca])){
                $this->view->assign('description',$descs[$mca]);
            }
            // keyword
            $keywords = array_column($pages,'keyword','mca');
            if(isset($keywords[$mca])){
                $this->view->assign('keywords',$keywords[$mca]);
            }
        }
     */

}
