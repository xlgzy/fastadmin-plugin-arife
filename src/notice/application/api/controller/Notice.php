<?php

namespace app\api\controller;

use app\common\controller\Api;

/**
 * 公告接口
 */
class Notice extends Api
{

    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = [];

    /**
     * 公告列表
     */
    public function list(){
        $limit = $this->request->post('limit',10);
        $res = db('Notice')->field('id,content')->order('createtime desc')->limit($limit)->select();
        $this->success('ok',$res);
    }

}
