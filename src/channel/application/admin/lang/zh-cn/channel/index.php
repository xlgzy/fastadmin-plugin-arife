<?php

return [
    'Id'         => 'ID',
    'Pid'        => '上级渠道ID',
    'Level'      => '渠道等级',
    'Admin_id'   => '关联账户',
    'Mobile'     => '手机号码',
    'Idcard'     => '身份证号码',
    'Ratio'      => '分佣比例',
    'Truename'   => '真实姓名',
    'Createtime' => '创建时间',
    'Status'     => '状态',
    'Status 0'   => '待审核',
    'Status 1'   => '已审核',
    'Status 2'   => '已驳回',
    'Remark'     => '备注',
    'Updatetime' => '更新时间',
    'Code'       => '识别码'
];
