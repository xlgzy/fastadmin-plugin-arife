<?php

namespace app\admin\model\channel;

use think\Model;


class Moneylog extends Model
{

    

    

    // 表名
    protected $name = 'channel_money_log';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function channel()
    {
        return $this->belongsTo('app\admin\model\channel\Index', 'channel_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    /**
     * 插入记录
     *
     * @param int $channel_id
     * @param float $total_fee
     * @param float $balance
     * @param string $memo
     * @param int $admin_id
     * @return void
     * @author AriFe.Liu 
     */
    public static function ins($channel_id,$total_fee,$balance,$memo,$admin_id){
        $res = self::insert([
            'channel_id' => $channel_id,
            'money' => -$total_fee,
            'before' => $balance,
            'after' => $balance-$total_fee,
            'memo' => $memo,
            'createtime' => time(),
            'admin_id' => $admin_id
        ]);
        if(!$res){
            exception('LOGING LOG HAS ERROR');
        }
    }
}
