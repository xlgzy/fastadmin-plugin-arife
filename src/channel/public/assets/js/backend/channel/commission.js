define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'channel/commission/index' + location.search,
                    add_url: 'channel/commission/add',
                    edit_url: 'channel/commission/edit',
                    del_url: 'channel/commission/del',
                    multi_url: 'channel/commission/multi',
                    import_url: 'channel/commission/import',
                    table: 'channel_commission',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'id', title: __('Id'), visible:false},
                        {field: 'channel.truename', title: __('渠道名称'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'total_fee', title: __('Total_fee'), operate:'BETWEEN'},
                        
                        {field: 'relinfo.title', title: __('订单名称'), operate: 'LIKE'},
                        {field: 'relinfo.total_fee', title: __('订单金额'), operate: 'LIKE'},
                        {field: 'relinfo.out_trade_no', title: __('订单编号'), operate: 'LIKE'},
                        {field: 'relinfo.nickname', title: __('用户昵称'), operate: 'LIKE'},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});