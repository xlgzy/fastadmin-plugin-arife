define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'channel/poster/index' + location.search,
                    add_url: 'channel/poster/add',
                    edit_url: 'channel/poster/edit',
                    del_url: 'channel/poster/del',
                    multi_url: 'channel/poster/multi',
                    import_url: 'channel/poster/import',
                    table: 'channel_poster',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title'), operate: 'LIKE'},
                        {field: 'poster_image', title: __('Poster_image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'url', title: __('Url'), operate: 'LIKE', formatter: Table.api.formatter.url},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1')}, formatter: Table.api.formatter.status},
                        {field: 'buttons',title: __('使用'),table:table,events: Table.api.events.operate,formatter:Table.api.formatter.buttons,buttons:[
                            {
                                name: 'copyurl',
                                text: __('链接'),
                                title: __('复制链接'),
                                classname: 'btn btn-xs btn-primary btn-ajax',
                                icon: 'fa fa-copy',
                                url: 'channel/Poster/copyurl',
                                success: function (data, ret) {
                                    if(ret.code == 1){
                                        var url = ret.data.url;
                                        $("body").after("<input id='copyVal'></input>");
                                        var text = url;
                                        var input = document.getElementById("copyVal");
                                        input.value = text;
                                        input.select();
                                        input.setSelectionRange(0, input.value.length);
                                        document.execCommand("copy");
                                        $("#copyVal").remove();
                                        layer.msg("复制成功");
                                    }else{
                                        layer.msg(ret.msg);
                                    }
                                    return false;
                                }
                            },
                            {
                                name: 'downloadposter',
                                text: __('海报'),
                                title: __('下载海报'),
                                classname: 'btn btn-xs btn-success btn-click',
                                icon: 'fa fa-download',
                                extend: ' target="_blank"',
                                url: 'channel/Poster/downloadPoster',
                                click: function(){
                                    window.location.href = this.href;
                                },
                            },
                            {
                                name: 'downloadqrcode',
                                text: __('二维码'),
                                title: __('下载二维码'),
                                classname: 'btn btn-xs btn-default btn-click',
                                icon: 'fa fa-download',
                                extend: ' target="_blank"',
                                url: 'channel/Poster/downloadQrcode',
                                click: function(){
                                    window.location.href = this.href;
                                },
                            }
                        ]},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});