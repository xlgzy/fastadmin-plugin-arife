<?php

return array (
  0 => 
  array (
    'name' => 'accesskey',
    'title' => 'AccessKey',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '',
    'rule' => 'required',
    'msg' => 'AccessKey必填',
    'tip' => '请填写阿里云申请到的AccessKey',
    'ok' => 'OK',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'accesskeysecret',
    'title' => 'AccessKeySecret',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '',
    'rule' => 'required',
    'msg' => 'AccessKeySecret必填',
    'tip' => '请填写阿里云申请到的AccessKeySecret',
    'ok' => 'OK',
    'extend' => '',
  ),
  2 => 
  array (
    'name' => 'signname',
    'title' => '签名',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '',
    'rule' => 'required',
    'msg' => '签名必填',
    'tip' => '请填写阿里云短信服务申请的签名',
    'ok' => 'OK',
    'extend' => '',
  ),
  3 => 
  array (
    'name' => 'templatecode',
    'title' => '模板Code',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '',
    'rule' => 'required',
    'msg' => '模板必填',
    'tip' => '请填写阿里云短信服务申请的模板',
    'ok' => 'OK',
    'extend' => '',
  ),
  4 => 
  array (
    'name' => '__tips__',
    'title' => '温馨提示',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '当前插件仅支持单模板的短信验证码发送服务',
    'rule' => '',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
