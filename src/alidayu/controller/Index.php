<?php
/*
 * @Author: AriFe.Liu
 * @Date: 2020-09-21 10:13:45
 * @LastEditors: AriFe.Liu
 * @LastEditTime: 2020-09-21 11:25:15
 * @Description: 
 */

namespace addons\alidayu\controller;

use think\addons\Controller;

class Index extends Controller
{

    public function index()
    {
        $this->error("当前插件暂无前台页面");
    }
}
