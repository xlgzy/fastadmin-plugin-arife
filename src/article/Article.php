<?php

namespace addons\article;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Article extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $menu = [
            [
                'name' => 'article',
                'title' => '文章管理', 
                'icon' => 'fa fa-list',
                'sublist' => [
                    [
                        'name' => 'article/category',
                        'title' => '文章分类',
                        'icon' => 'fa fa-align-justify',
                        'sublist' => [
                            [
                                'name' => 'article/category/index',
                                'title' => '查看',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/category/recyclebin',
                                'title' => '回收站',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/category/add',
                                'title' => '添加',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/category/edit',
                                'title' => '编辑',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/category/del',
                                'title' => '删除',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/category/destroy',
                                'title' => '真实删除',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/category/restore',
                                'title' => '还原',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/category/multi',
                                'title' => '批量更新',
                                'ismenu' => 0
                            ],
                        ]
                    ],
                    [
                        'name' => 'article/article',
                        'title' => '文章管理',
                        'icon' => 'fa fa-align-justify',
                        'sublist' => [
                            [
                                'name' => 'article/article/index',
                                'title' => '查看',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/article/recyclebin',
                                'title' => '回收站',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/article/add',
                                'title' => '添加',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/article/edit',
                                'title' => '编辑',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/article/del',
                                'title' => '删除',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/article/destroy',
                                'title' => '真实删除',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/article/restore',
                                'title' => '还原',
                                'ismenu' => 0
                            ],
                            [
                                'name' => 'article/article/multi',
                                'title' => '批量更新',
                                'ismenu' => 0
                            ],
                        ]
                    ],
                ]
            ]
        ];
        Menu::create($menu);
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        Menu::delete('article');
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        Menu::enable('article');
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        Menu::disable('article');
        return true;
    }

}
