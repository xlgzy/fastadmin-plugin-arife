<?php

return [
    'Id'                  => 'ID',
    'Title'               => '文章标题',
    'Subtitle'            => '副标题',
    'Article_category_id' => '文章分类',
    'Content'             => '正文内容',
    'Weigh'               => '权重',
    'Createtime'          => '创建时间',
    'Updatetime'          => '更新时间',
    'Deletetime'          => '删除时间',
    'Category.title'      => '分类名称'
];
