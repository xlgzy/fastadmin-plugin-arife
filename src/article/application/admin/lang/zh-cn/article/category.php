<?php

return [
    'Id'         => 'ID',
    'Title'      => '分类名称',
    'Status'     => '状态',
    'Status 0'   => '隐藏',
    'Set status to 0'=> '设为隐藏',
    'Status 1'   => '显示',
    'Set status to 1'=> '设为显示',
    'Weigh'      => '权重',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间'
];
